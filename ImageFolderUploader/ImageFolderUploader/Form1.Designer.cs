﻿namespace ImageFolderUploader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelFolder = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_SelectFolder = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_BrowseFolder = new System.Windows.Forms.Button();
            this.txt_FolderName = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelUserName = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelPassword = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.btn_submit = new System.Windows.Forms.Button();
            this.lbl_name = new System.Windows.Forms.Label();
            this.grid_ImageUploader = new System.Windows.Forms.DataGridView();
            this.browse_Folder = new System.Windows.Forms.FolderBrowserDialog();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewImageColumn();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelFolder.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelUserName.SuspendLayout();
            this.tableLayoutPanelPassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_ImageUploader)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelFolder, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelUserName, 0, 4);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelPassword, 0, 5);
            this.tableLayoutPanelMain.Controls.Add(this.btn_submit, 0, 6);
            this.tableLayoutPanelMain.Controls.Add(this.lbl_name, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.grid_ImageUploader, 0, 3);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 8;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(684, 462);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // tableLayoutPanelFolder
            // 
            this.tableLayoutPanelFolder.ColumnCount = 2;
            this.tableLayoutPanelFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.46746F));
            this.tableLayoutPanelFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.53255F));
            this.tableLayoutPanelFolder.Controls.Add(this.lbl_SelectFolder, 0, 0);
            this.tableLayoutPanelFolder.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanelFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelFolder.Location = new System.Drawing.Point(4, 73);
            this.tableLayoutPanelFolder.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanelFolder.Name = "tableLayoutPanelFolder";
            this.tableLayoutPanelFolder.RowCount = 1;
            this.tableLayoutPanelFolder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelFolder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanelFolder.Size = new System.Drawing.Size(676, 38);
            this.tableLayoutPanelFolder.TabIndex = 0;
            // 
            // lbl_SelectFolder
            // 
            this.lbl_SelectFolder.AutoSize = true;
            this.lbl_SelectFolder.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbl_SelectFolder.Location = new System.Drawing.Point(138, 12);
            this.lbl_SelectFolder.Margin = new System.Windows.Forms.Padding(4, 12, 4, 0);
            this.lbl_SelectFolder.Name = "lbl_SelectFolder";
            this.lbl_SelectFolder.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_SelectFolder.Size = new System.Drawing.Size(91, 26);
            this.lbl_SelectFolder.TabIndex = 0;
            this.lbl_SelectFolder.Text = "Select Folder";
            this.lbl_SelectFolder.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn_BrowseFolder);
            this.flowLayoutPanel1.Controls.Add(this.txt_FolderName);
            this.flowLayoutPanel1.Controls.Add(this.btn_refresh);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(237, 4);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(435, 30);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // btn_BrowseFolder
            // 
            this.btn_BrowseFolder.Location = new System.Drawing.Point(0, 0);
            this.btn_BrowseFolder.Margin = new System.Windows.Forms.Padding(0);
            this.btn_BrowseFolder.Name = "btn_BrowseFolder";
            this.btn_BrowseFolder.Size = new System.Drawing.Size(90, 30);
            this.btn_BrowseFolder.TabIndex = 2;
            this.btn_BrowseFolder.Text = "Browse";
            this.btn_BrowseFolder.UseVisualStyleBackColor = true;
            this.btn_BrowseFolder.Click += new System.EventHandler(this.btn_BrowseFolder_Click);
            // 
            // txt_FolderName
            // 
            this.txt_FolderName.Location = new System.Drawing.Point(97, 0);
            this.txt_FolderName.Margin = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.txt_FolderName.Multiline = true;
            this.txt_FolderName.Name = "txt_FolderName";
            this.txt_FolderName.ReadOnly = true;
            this.txt_FolderName.Size = new System.Drawing.Size(232, 30);
            this.txt_FolderName.TabIndex = 3;
            // 
            // tableLayoutPanelUserName
            // 
            this.tableLayoutPanelUserName.ColumnCount = 2;
            this.tableLayoutPanelUserName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelUserName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelUserName.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanelUserName.Controls.Add(this.txt_username, 1, 0);
            this.tableLayoutPanelUserName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelUserName.Location = new System.Drawing.Point(4, 303);
            this.tableLayoutPanelUserName.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanelUserName.Name = "tableLayoutPanelUserName";
            this.tableLayoutPanelUserName.RowCount = 1;
            this.tableLayoutPanelUserName.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUserName.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanelUserName.Size = new System.Drawing.Size(676, 38);
            this.tableLayoutPanelUserName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Location = new System.Drawing.Point(259, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 12, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "UserName";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txt_username
            // 
            this.txt_username.Location = new System.Drawing.Point(342, 4);
            this.txt_username.Margin = new System.Windows.Forms.Padding(4);
            this.txt_username.Multiline = true;
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(330, 30);
            this.txt_username.TabIndex = 1;
            // 
            // tableLayoutPanelPassword
            // 
            this.tableLayoutPanelPassword.ColumnCount = 2;
            this.tableLayoutPanelPassword.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelPassword.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelPassword.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanelPassword.Controls.Add(this.txt_password, 1, 0);
            this.tableLayoutPanelPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelPassword.Location = new System.Drawing.Point(4, 349);
            this.tableLayoutPanelPassword.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanelPassword.Name = "tableLayoutPanelPassword";
            this.tableLayoutPanelPassword.RowCount = 1;
            this.tableLayoutPanelPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanelPassword.Size = new System.Drawing.Size(676, 38);
            this.tableLayoutPanelPassword.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Location = new System.Drawing.Point(265, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 12, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 26);
            this.label2.TabIndex = 0;
            this.label2.Text = "Password";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txt_password
            // 
            this.txt_password.Location = new System.Drawing.Point(342, 4);
            this.txt_password.Margin = new System.Windows.Forms.Padding(4);
            this.txt_password.Multiline = true;
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(330, 30);
            this.txt_password.TabIndex = 1;
            // 
            // btn_submit
            // 
            this.btn_submit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_submit.Location = new System.Drawing.Point(275, 395);
            this.btn_submit.Margin = new System.Windows.Forms.Padding(4);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(133, 32);
            this.btn_submit.TabIndex = 4;
            this.btn_submit.Text = "Submit";
            this.btn_submit.UseVisualStyleBackColor = true;
            // 
            // lbl_name
            // 
            this.lbl_name.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl_name.AutoSize = true;
            this.lbl_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(104, 23);
            this.lbl_name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(476, 39);
            this.lbl_name.TabIndex = 5;
            this.lbl_name.Text = "Select Folder To Upload Files.";
            // 
            // grid_ImageUploader
            // 
            this.grid_ImageUploader.AllowUserToAddRows = false;
            this.grid_ImageUploader.AllowUserToDeleteRows = false;
            this.grid_ImageUploader.AllowUserToResizeRows = false;
            this.grid_ImageUploader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grid_ImageUploader.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid_ImageUploader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_ImageUploader.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.FileName,
            this.Status});
            this.grid_ImageUploader.Location = new System.Drawing.Point(4, 119);
            this.grid_ImageUploader.Margin = new System.Windows.Forms.Padding(4);
            this.grid_ImageUploader.Name = "grid_ImageUploader";
            this.grid_ImageUploader.RowTemplate.Height = 30;
            this.grid_ImageUploader.Size = new System.Drawing.Size(676, 176);
            this.grid_ImageUploader.TabIndex = 3;
            // 
            // btn_refresh
            // 
            this.btn_refresh.Location = new System.Drawing.Point(332, 3);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(75, 28);
            this.btn_refresh.TabIndex = 4;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // SrNo
            // 
            this.SrNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SrNo.FillWeight = 63.64111F;
            this.SrNo.HeaderText = "Sr. No.";
            this.SrNo.MinimumWidth = 50;
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Width = 77;
            // 
            // FileName
            // 
            this.FileName.FillWeight = 185.562F;
            this.FileName.HeaderText = "File Name";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Status.FillWeight = 28.15854F;
            this.Status.HeaderText = "File Status";
            this.Status.MinimumWidth = 20;
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Status.Width = 99;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 462);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(800, 500);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Uploader";
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.tableLayoutPanelFolder.ResumeLayout(false);
            this.tableLayoutPanelFolder.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.tableLayoutPanelUserName.ResumeLayout(false);
            this.tableLayoutPanelUserName.PerformLayout();
            this.tableLayoutPanelPassword.ResumeLayout(false);
            this.tableLayoutPanelPassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_ImageUploader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelFolder;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelUserName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPassword;
        private System.Windows.Forms.Label lbl_SelectFolder;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btn_BrowseFolder;
        private System.Windows.Forms.TextBox txt_FolderName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.DataGridView grid_ImageUploader;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.FolderBrowserDialog browse_Folder;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewImageColumn Status;
    }
}


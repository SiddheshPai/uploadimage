﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ImageFolderUploader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_BrowseFolder_Click(object sender, EventArgs e)
        {
           
            try
            {
                if (browse_Folder.ShowDialog() == DialogResult.OK)
                    CheckFileFormat();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void CheckFileFormat()
        {
            DirectoryInfo Folder = null;
            List<FileInfo> Files = null;
            int SrNo = 1;
            string FileName = string.Empty;
            try
            {
                if (grid_ImageUploader.Rows.Count > 0)
                    grid_ImageUploader.Rows.Clear();
                // Showing Full Path.
                //txt_FolderName.Text = browse_Folder.SelectedPath.ToString();

                txt_FolderName.Text = browse_Folder.SelectedPath.Substring(browse_Folder.SelectedPath.LastIndexOf('\\') + 1).ToString();
                Folder = new DirectoryInfo(browse_Folder.SelectedPath.ToString());
                Files = new List<FileInfo>();
                Files = Folder.GetFiles("*.jpg").ToList();
                if (Files.Count <= 0)
                {
                    MessageBox.Show("This folder does not contain any images.");
                }
                else
                {
                    foreach (FileInfo file in Files)
                    {
                        FileName = file.Name.Replace(".jpg", "");
                        // name patern 
                        //if (!Regex.IsMatch(FileName, @"^\d{1,2}.\d{1,2}-\w{1}-\d{1,2}$"))
                        if (!Regex.IsMatch(FileName, @"^\w{1,5}-\w{1}-\w{1}-\d{1,3}$"))
                        {
                            grid_ImageUploader.Rows.Add(SrNo.ToString(), file.FullName, Properties.Resources.Wrong);
                        }
                        else
                        {
                            grid_ImageUploader.Rows.Add(SrNo.ToString(), file.FullName, Properties.Resources.Right);
                        }
                        SrNo++;
                    }
                    if (grid_ImageUploader.Rows.Count == 0)
                        MessageBox.Show("Images Uploaded Successfully.");
                    else
                        MessageBox.Show("Please Rename Files In This 'HS020-S-A-45.jpg' Format.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            CheckFileFormat();
        }
    }
}
